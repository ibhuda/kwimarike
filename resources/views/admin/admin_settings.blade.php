@extends('layouts.admin_layout.admin_layout')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Settings</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Admin Settings</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Update Password</h3>
                        </div>
                        <!-- /.card-header -->
                        @if(Session::has('error_message'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert"
                            style="margin-top: 10px;">
                            {{Session::get('error_message')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif

                        @if(Session::has('success_message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert"
                            style="margin-top: 10px;">
                            {{Session::get('success_message')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif

                        <!-- form start -->
                        <form role="form" method="POST" action="{{ url('/admin/update-current-password')}}"
                            name="updatePassword" id="updatePasswordForm">@csrf
                            <div class="card-body">
                                <?php /*<div class="form-group">
                    <label for="name">Admin Name </label>
                    <input type="name" class="form-control" value="{{ $adminDetails->name }}" type="text" placeholder="Enter Admin Name" id="admin">
                  </div> */ ?>
                                <div class="form-group">
                                    <label for="Email">Admin Email </label>
                                    <input type="email" class="form-control" value="{{ $adminDetails->email }}"
                                        readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="type">Admin Type</label>
                                    <input type="type" class="form-control" value="{{ $adminDetails->type }}"
                                        readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="current_password">Current Password</label>
                                    <input type="password" name="current_password" class="form-control"
                                        id="current_password" placeholder="Enter Current Password" required="">
                                    <span id="checkCurrentPwd"></span>
                                </div>
                                <div class="form-group">
                                    <label for="new_password">New Password</label>
                                    <input type="password" name="new_password" class="form-control" id="new_password"
                                        placeholder="Enter New Password" required="">
                                </div>
                                <div class="form-group">
                                    <label for="confirm">Confirm Password</label>
                                    <input type="password" name="confirm_password" class="form-control"
                                        id="confirm_password" placeholder="Confirm Password" required="">
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- /.card -->

            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection