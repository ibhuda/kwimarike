<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->delete();
        $adminRecords = [
        
            ['id' =>1,'name'=>'admin','type' =>'admin','mobile'=>'0123456789', 'email'=>'admin@admin.com',
            'password'=>'$2y$10$sId5aezHTQuG8R5dwM4H5.RPgosOgXDIaW0G30Re3l5mmQD/PzUOu','image'=>'','status'=>1
        ],

        ];

        DB::table('admins')->insert($adminRecords);

        // foreach($adminRecords as $key => $record){
            // \App\Admin::create($record);
        }
    
}
